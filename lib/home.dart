import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

export 'package:netflix_clone/home.dart' show HomePage;

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> _globalKey =
      GlobalKey(debugLabel: "test scaffoldState");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _globalKey,
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              _buildMajorRecommendedVideo(context),
              _buildRecommendedMenu("現正熱播"),
              _buildRecommendedMenu("推薦影片"),
              _buildTop10Menu("榮登本日台灣排行榜前10名")
            ],
          ),
        ));
  }

  Widget _buildMajorRecommendedVideo(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .7,
      child: Stack(
        fit: StackFit.expand,
        children: [
          ShaderMask(
            shaderCallback: (bounds) {
              return LinearGradient(
                colors: [Colors.white, Colors.white, Colors.transparent],
                stops: [0.0, 0.7, 1.0],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ).createShader(bounds);
            },
            child: Image.asset('assets/videos1.jpg', fit: BoxFit.fitHeight),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    Text(
                      "黑暗 ",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text("刺激 ", style: Theme.of(context).textTheme.bodyText1),
                    Text("奇幻動畫 ", style: Theme.of(context).textTheme.bodyText1),
                    Text("動作動畫 ", style: Theme.of(context).textTheme.bodyText1),
                    Text("神話與傳說 ",
                        style: Theme.of(context).textTheme.bodyText1),
                    Text("報復 ", style: Theme.of(context).textTheme.bodyText1),
                  ],
                ),
                SizedBox(
                  height: 24.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                        Text(
                          "我的片單",
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        SizedBox(
                          width: 80,
                        )
                      ],
                    ),
                    Container(
                      width: 90.0,
                      color: Colors.white,
                      padding: EdgeInsets.all(4.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Icon(
                            Icons.play_arrow,
                            size: 30,
                            color: Colors.black,
                          ),
                          Text(
                            "播放",
                            style: TextStyle(color: Colors.black, fontSize: 18),
                          )
                        ],
                      ),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.info_outline,
                          color: Colors.white,
                        ),
                        Text("資訊",
                            style: Theme.of(context).textTheme.bodyText1),
                        SizedBox(
                          width: 80.0,
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildRecommendedMenu(String title, {List playList}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 24.0,
        ),
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            title,
            style: TextStyle(fontSize: 24.0, color: Colors.white),
          ),
        ),
        SizedBox(
          height: 150,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: List.generate(8, (index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Ink(
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      image: DecorationImage(
                          image: AssetImage("assets/videos1.jpg"),
                          fit: BoxFit.cover)),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(8.0),
                    onLongPress: () {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(SnackBar(content: Text("動畫名字")));
                    },
                  ),
                ),
              );
            }),
          ),
        )
      ],
    );
  }

  Widget _buildTop10Menu(String title, {List playList}) {
    Widget _top10(int index) {
      return SizedBox(
        width: 120,
        child: Stack(
          alignment: AlignmentDirectional.bottomStart,
          children: [
            Transform.translate(
              offset: Offset(-14.0, 0),
              child: Text(
                "${index + 1}",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 100.0,
                    fontWeight: FontWeight.w700,
                    height: 0.9,
                    letterSpacing: 0.0,
                    wordSpacing: 0.0,
                    shadows: [
                      Shadow(offset: Offset(-1.5, -1.5), color: Colors.white),
                      Shadow(offset: Offset(1.5, -1.5), color: Colors.white),
                      Shadow(offset: Offset(1.5, 1.5), color: Colors.white),
                      Shadow(offset: Offset(-1.5, 1.5), color: Colors.white)
                    ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Ink(
                width: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    image: DecorationImage(
                        image: AssetImage("assets/videos1.jpg"),
                        fit: BoxFit.cover)),
                child: InkWell(
                  borderRadius: BorderRadius.circular(8.0),
                  onLongPress: () {
                    ScaffoldMessenger.of(context)
                        .showSnackBar(SnackBar(content: Text("動畫名字")));
                  },
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 24.0,
        ),
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            title,
            style: TextStyle(fontSize: 24.0, color: Colors.white),
          ),
        ),
        SizedBox(
          height: 150,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: List.generate(
              8,
              (index) => index == 0
                  ? Padding(
                      padding: EdgeInsets.only(left: 10.0),
                      child: _top10(index),
                    )
                  : ClipRRect(
                      child: _top10(index),
                    ),
            ),
          ),
        ),
      ],
    );
  }
}
